export class Annonce {
    titre: string;

    json() {
        return {
            titre: this.titre
        }
    }
}
