import { Component, OnInit } from '@angular/core';
import { Annonce } from 'src/app/model/annonce';

@Component({
  selector: 'app-nouvelle-annonce',
  templateUrl: './nouvelle.component.html',
  styleUrls: ['./nouvelle.component.scss']
})
export class NouvelleAnnonceComponent implements OnInit {
  annonce = new Annonce();

  constructor() { }

  ngOnInit() {

  }

}
