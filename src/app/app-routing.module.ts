import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NouvelleAnnonceComponent } from './view/annonces/nouvelle/nouvelle.component';


const routes: Routes = [
  { path: 'annonces/nouvelle', component: NouvelleAnnonceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
